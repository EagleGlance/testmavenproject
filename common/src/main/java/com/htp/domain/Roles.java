package com.htp.domain;

public enum Roles {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_MANAGER
}


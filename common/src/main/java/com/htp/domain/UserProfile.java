package com.htp.domain;

import lombok.Data;

@Data
public class UserProfile {
    private Long id;

    private String username;

    private String surname;

    private String login;

}
